/*
 * Compile with:
 *  valac --pkg appindicator3-0.1 --pkg gtk+-3.0 sample.vala
 */

using AppIndicator;
using Gtk;

public class IndicatorExample {
        public static int main(string[] args) {
                Gtk.init(ref args);

                var win = new Gtk.Window();
                win.title = "Indicator Test";
                win.resize(200, 200);
                win.destroy.connect(Gtk.main_quit);

                var label = new Gtk.Label("Hello, world!");
                win.add(label);

                var indicator = new Indicator(win.title, "",
                                              IndicatorCategory.APPLICATION_STATUS);

                indicator.set_status(IndicatorStatus.ACTIVE);
                
                var prog_path = GLib.Path.get_dirname( GLib.FileUtils.read_link("/proc/self/exe") );
				var icon_path = GLib.Path.build_path(GLib.Path.DIR_SEPARATOR_S,prog_path,"hicolor","48x48","status");
                indicator.set_icon_theme_path(icon_path);
                indicator.set_attention_icon_full("messages-new","new");
                indicator.set_icon_full("messages","origin");
                
                print( indicator.get_icon_theme_path () +"\n" );

                var menu = new Gtk.Menu();

                var item = new Gtk.MenuItem.with_label("Foo");
                item.activate.connect(() => {
                        indicator.set_status((indicator.get_status())%2 + 1);
                });
                item.show();
                menu.append(item);

                item = new Gtk.MenuItem.with_label("Bar");
                item.show();
                item.activate.connect(() => {
                        indicator.set_status(IndicatorStatus.ATTENTION);
                });
                menu.append(item);

                indicator.set_menu(menu);

                win.show_all();

                Gtk.main();
                return 0;
        }
}
