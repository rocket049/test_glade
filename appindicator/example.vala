using AppIndicator;

public class IndicatorExample {
        public static int main(string[] args) {
                Gtk.init(ref args);

                var win = new Gtk.Window();
                win.title = "Indicator Test";
                win.resize(200, 200);
                win.destroy.connect(Gtk.main_quit);

                var label = new Gtk.Label("Hello, world!");
                win.add(label);

                var indicator = new Indicator(win.title, "act",
                                              IndicatorCategory.APPLICATION_STATUS);

                indicator.set_status(IndicatorStatus.ACTIVE);
                var prog_path = GLib.Path.get_dirname( GLib.FileUtils.read_link("/proc/self/exe") );
                indicator.set_icon_theme_path(prog_path);
                indicator.set_attention_icon("atn");
                //indicator.set_icon("act");

                var menu = new Gtk.Menu();

                var item = new Gtk.MenuItem.with_label("Foo");
                item.activate.connect(() => {
                        indicator.set_status(IndicatorStatus.ATTENTION);
                });
                item.show();
                menu.append(item);

                item = new Gtk.MenuItem.with_label("Bar");
                item.show();
                item.activate.connect(() => {
                        indicator.set_icon_full("act","act");
                        indicator.set_label("act","label act");
                        indicator.set_status(IndicatorStatus.ACTIVE);
                });
                menu.append(item);
                
                item = new Gtk.MenuItem.with_label("Ppp");
                item.show();
                item.activate.connect(() => {
                        indicator.set_icon_full("ppp","ppp");
                        indicator.set_status(IndicatorStatus.ACTIVE);
                });
                menu.append(item);

                indicator.set_menu(menu);

                win.show_all();

                Gtk.main();
                return 0;
        }
}
