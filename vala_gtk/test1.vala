using Gtk;

int main (string[] args) {
    Gtk.init (ref args);
	var builder1 = new Gtk.Builder.from_file("./test1.ui");
	var appwin1 = (Gtk.Window)builder1.get_object("appwin1");
	appwin1.destroy.connect ( ()=>{
		stdout.printf("Quit normal.\n");
		Gtk.main_quit();
	});
	appwin1.enter_notify_event.connect( ()=>{
			Idle.add( ()=>{
				stdout.printf("enter.\n");
				return false; //if true, will repeat run every glib idle
			});
			return true;
	});
	appwin1.leave_notify_event.connect( ()=>{
			Idle.add( ()=>{
				stdout.printf("leave.\n");
				return false;  //if true, will repeat run every glib idle
			});
			return true;
	});
	appwin1.hide.connect( ()=>{
			Idle.add( ()=>{
				stdout.printf("hide.\n");
				return true;
			});
			//return true;
	});
    
    Gtk.main();
    return 0;
}
