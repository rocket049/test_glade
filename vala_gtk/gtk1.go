package main

import (
	"fmt"
	"os"
	"path"
	"reflect"

	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
)

func main() {
	//var appwin1, cmd1 glib.IObject
	var builder *gtk.Builder
	builder, _ = gtk.BuilderNew()
	gtk.Init(&os.Args)
	err := builder.AddFromFile(path.Join(os.Getenv("HOME"), "golang/gtkui/test1.ui"))
	if err != nil {
		panic(err)
	}
	iobj1, _ := builder.GetObject("appwin1")
	//appwin1,ok := iobj1.(*gtk.Window)
	fmt.Println("appwin1", reflect.TypeOf(iobj1))
	iobj2, _ := builder.GetObject("cmd1")
	//cmd1,ok2 := iobj2.(*gtk.TextView)
	fmt.Println("cmd1", reflect.TypeOf(iobj2))

	/*
	 * func (v *Object) Connect(detailedSignal string, f interface{}, userData ...interface{}) (SignalHandle, error)
	 * */
	var handler1 = AppHandler{builder}
	var funcmap = make(map[string]interface{})
	funcmap["on_destroy"] = handler1.onDestroy
	funcmap["on_input"] = handler1.onInput
	funcmap["on_paste"] = handler1.onPaste
	funcmap["on_cursor_move"] = handler1.onCursorMove
	//appwin1.Connect("destroy",onDestroy)
	//cmd1.Connect("key-release-event",onInput)
	//cmd1.Connect("button-release-event",onCursorMove)
	builder.ConnectSignals(funcmap)
	gtk.Main()
}

type AppHandler struct {
	Builder *gtk.Builder
}

func (self *AppHandler) onDestroy() {
	fmt.Println("call onDestroy")
	gtk.MainQuit()
}

func (self *AppHandler) onInput(widget interface{}, event interface{}) {
	fmt.Println("call onInput", event)
}

func (self *AppHandler) onPaste(event interface{}) {
	fmt.Println("call onPaste", reflect.TypeOf(event))
}

func (self *AppHandler) onCursorMove(widget *gtk.TextView, event *gdk.Event) {
	fmt.Println("call onCursorMove")
}
