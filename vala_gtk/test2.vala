using Gtk;

int main (string[] args) {
    Gtk.init (ref args);
	var builder1 = new Gtk.Builder.from_file("/home/fhz8/golang/gtkui/web1.ui");
	var win1 = (Gtk.Window)builder1.get_object("window1");
	var frame1 = (Gtk.Frame)builder1.get_object("webkit1");
	var web1 = new WebKit.WebView();
	web1.set_visible(true);
	frame1.add(web1);
	frame1.show_all();
	win1.destroy.connect ( ()=>{
		stdout.printf("Quit normal.\n");
		Gtk.main_quit();
	});
	win1.enter_notify_event.connect( ()=>{
			Idle.add( ()=>{
				stdout.printf("enter.\n");
				return false; //if true, will repeat run every glib idle
			});
			return true;
	});
	win1.leave_notify_event.connect( ()=>{
			Idle.add( ()=>{
				stdout.printf("leave.\n");
				return false;  //if true, will repeat run every glib idle
			});
			return true;
	});
	
    web1.load_uri("http://127.0.0.1/share/");
    Gtk.main();
    return 0;
}
