//server.go
package main

import (
	"fmt"
	"log"
	"net"
	"net/rpc"
	"os"
	"os/signal"
	"strings"

	"github.com/rocket049/go-jsonrpc2glib"
)

type Arith int

type ParamsT struct {
	Arg string
}

func (t *Arith) Hello(args []string, reply *string) error {
	*reply = strings.Join(args, "\n")
	return nil
}

func main() {
	arith := new(Arith)
	rpc.Register(arith)
	l, e := net.Listen("tcp", ":6666")
	defer l.Close()
	if e != nil {
		log.Fatal("listen error:", e)
	}
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println(err)
		}
		go jsonrpc2glib.ServeGlib(conn, nil)
	}

	waitSig()
}

func waitSig() {
	var c chan os.Signal = make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	s := <-c
	fmt.Println("\nSignal:", s)
}
