//client.vala
//valac --pkg jsonrpc-glib-1.0 jsonclient.vala
using Jsonrpc;

owned SocketConnection rpcConnect(string host,uint16 port){
	Resolver resolver = Resolver.get_default ();
	List<InetAddress> addresses = resolver.lookup_by_name (host, null);
	InetAddress address = addresses.nth_data (0);
	SocketClient client = new SocketClient ();
	SocketConnection conn = client.connect(new InetSocketAddress (address, port));
	return conn;
}

void rpcClient(SocketConnection conn){
	var c = new Jsonrpc.Client(conn);
	string[] v = {"Hello friend.","无限恐怖","abcde"};
	var params = new Variant.strv(v);
	Variant res;
	try{
		for (int i=0;i<5;i++){
			var ok = c.call("Arith.Hello",params,null,out res);
			if(ok){
				stdout.printf("%s\n",res.get_string());
			}else{
				stdout.printf("error\n");
			}
		}
	}catch (Error e) {
		stdout.printf ("Error: %s\n", e.message);
	}
	conn.close();
}

void main(){
	var conn = rpcConnect("localhost",6666);
	rpcClient(conn);
	stdout.printf("end\n");
}
