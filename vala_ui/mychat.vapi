[CCode (cheader_filename="libmychat.h")]

public static int GetMsg(out string p0, out string p1);

public static void ChatInit(int p0);

public static int SetIRCServer(string p0);

public static int SetNick(string p0);

public static int SetRoom(string p0, out string p1);

public static int SetKey(string p0);

public static string GetHead(int p0);

public static void Quit();

public static void Say(string p0, string p1);

public static int GetStep();
