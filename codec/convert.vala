//using Gtk;
public string? to_utf8(string s){
	string codec;
	if(GLib.get_charset(out codec)){
		return s;
	}else{
		return GLib.convert_with_fallback(s,s.length,"UTF-8", codec);
	}
}

public string? to_local(string s){
	string codec;
	if(GLib.get_charset(out codec)){
		return s;
	}else{
		return GLib.convert(s,s.length, codec,"UTF-8");
	}
}

void to_uri(string path1){
	stdout.printf("%s\n", path1 );
	var uri1 = GLib.Filename.to_uri(path1);
	stdout.printf("%s\n", uri1 );
}

public static void main(string[] args){
	GLib.Intl.setlocale(GLib.LocaleCategory.ALL,"");

	stdout.printf("%s\n",Environment.get_variable("LANG"));
	string codec;
	GLib.get_charset(out codec);
	stdout.printf(@"charset: $(codec)\n");
	
	to_uri("/a/b/c.txt");
	
	if(args.length<2){
		return;
	}else{
		var s8 = to_utf8(args[1]);
		stdout.printf("utf8: %s\n",s8);
		stdout.printf("local: %s\n",to_local(s8));
	}
}
