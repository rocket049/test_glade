#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>

void user_input1(GtkWidget *widget, GdkEvent *event, gpointer user_data);
void user_destroy1(GtkWidget *object, gpointer user_data);
GtkBuilder *builder;
void cursor_moved (GtkWidget *widget, GdkEvent  *event, gpointer   user_data);

int main(int argc, char *argv[])
{
    extern GtkBuilder *builder;
    GObject *appwin1, *cmd1;
    builder = gtk_builder_new();
    gtk_init(&argc,&argv);
    gtk_builder_add_from_file(builder,"test1.ui",NULL);
    appwin1 = gtk_builder_get_object(builder,"appwin1");
    cmd1    = gtk_builder_get_object(builder,"cmd1");
    g_signal_connect(appwin1,"destroy",G_CALLBACK(gtk_main_quit),NULL);
    g_signal_connect(cmd1,"key-release-event",G_CALLBACK(user_input1),"input");
    g_signal_connect(cmd1,"button-release-event",G_CALLBACK(cursor_moved),"cursor");
    gtk_main();
    return 0;
}

gboolean cursor_at_end(GtkTextView *widget)
{
    GdkRectangle strong,endp;
    GtkTextIter iter1;
    gtk_text_view_get_cursor_locations(widget, NULL, &strong,NULL);
    gtk_text_buffer_get_end_iter(gtk_text_view_get_buffer(widget), &iter1);
    gtk_text_view_get_iter_location (widget,&iter1,&endp);
    if(strong.x==endp.x && strong.y==endp.y){
        return TRUE;
    }else{
        return FALSE;
    }
}

void user_input1(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
    GdkEventKey *e = (GdkEventKey *)event;
    GdkRectangle strong;
    GtkTextIter iter1,iter2;
    GtkTextBuffer *buf1;
    GtkTextMark *mark1;
    gint top1;
    gchar *txt1;
    gchar *rsp;
    gboolean editable;
    extern GtkBuilder *builder;
    GtkStatusbar *stat1 = (GtkStatusbar *)gtk_builder_get_object(builder,"stat1");
    editable=cursor_at_end((GtkTextView *)widget);
    gtk_text_view_set_editable( (GtkTextView *)widget, editable);
    if(!editable){
        goto endsw;
    }
    switch (e->keyval){
        case 65293:
            /*得到光标位置*/
            gtk_text_view_get_cursor_locations((GtkTextView *)widget, NULL, &strong,NULL);
            /*倒数第二行*/
            gtk_text_view_get_line_at_y ((GtkTextView *)widget, &iter1,strong.y-strong.height,&top1);
            /*最后一行*/
            gtk_text_view_get_line_at_y ((GtkTextView *)widget, &iter2,strong.y,&top1);
            txt1 = gtk_text_iter_get_text(&iter1,&iter2);
            if( strlen(txt1)>1 ){
                rsp = g_strdup_printf("接受命令：%s",txt1);
                g_free(txt1);
                /*插入反馈信息*/
                buf1=gtk_text_view_get_buffer((GtkTextView *)widget);
                gtk_text_buffer_insert (buf1, &iter2,\
                            rsp,strlen(rsp) ); 
                g_free(rsp);
                /*窗口滚动*/
                gtk_text_buffer_get_end_iter(buf1, &iter2);
                mark1 = gtk_text_buffer_create_mark(buf1,NULL,&iter2,1);
                gtk_text_view_scroll_to_mark((GtkTextView *)widget,mark1,0,1,1,1);
            }
            goto endsw;
        default:
            break;
    }
    endsw:
    /*显示状态栏信息*/
    rsp = g_strdup_printf("key value: %d  hwkeycode: %d",e->keyval,e->hardware_keycode);
    gtk_statusbar_push(stat1,0,rsp);
    g_free(rsp);
}

void user_destroy1(GtkWidget *object,gpointer   user_data)
{
    return;
}

void cursor_moved (GtkWidget *widget, GdkEvent  *event, gpointer   user_data )
{
    gtk_text_view_set_editable( (GtkTextView *)widget,cursor_at_end((GtkTextView *)widget) );
}
