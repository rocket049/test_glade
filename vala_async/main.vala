// Example with GIO asynchronous methods:
// Build with: valac --pkg=gio-2.0 example.vala

void wait(int n) {
    GLib.Thread.usleep(n*1000000);
}
async int example_async() {
    SourceFunc callback = example_async.callback;
    //异步其实就是线程！
    ThreadFunc<bool> run = () => {
        //需要异步运行的代码
        wait(2);
        print("end\n");
        
        //线程结束后的调用
        Idle.add((owned) callback);
        return true;
    };
    new Thread<bool>("thread-example", run);
    // Wait for background thread to schedule our callback
    yield;
    return 1;
}

void main() {
    var loop = new MainLoop();
    example_async.begin((obj, res) => {
            var ret=example_async.end(res);
            print(@"result:$(ret)\n");
            loop.quit();
        });
    print("wait\n");
    loop.run();
}
