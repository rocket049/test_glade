using Gee;

public class TestClass:GLib.Object{
	public static int count{get;private set;default=8;}
	public string name{get;private set;}
    public weak TestClass pre;
    public TestClass next;
	public TestClass(string name1){
		this.name = name1;
	}
	public void Show(){
		stdout.printf("%s %d\n",this.name,count);
		count++;
	}
}

class TestS1 {
    public int id;
}

void tryTestS1(){
    //初始化了指针数组，但是没有初始化 class
    //TestS1[5] h =  new TestS1[5];
    
    var l = new Gee.ArrayList<TestS1>();
    
    for(int i=0;i<6;i++){
        //h[i]=new TestS1();
        //h[i].id=i;
        var e = new TestS1();
        e.id = i;
        l.add(e);
    }
    
    for(int i=0;i<l.size;i++){
        stdout.printf("struct id: %d\n" ,l[i].id);
    }
}

void main(){
	var t1 = new TestClass("test1");
	t1.next = new TestClass("test2");
    t1.pre = t1.next;
    t1.next.pre = t1;
    t1.next.next = t1;
    weak TestClass p = t1;
	for (int i=0;i<6;i++){
		p.Show();
		p = p.next;
        //stdout.printf("next:%d\n",TestClass.count);
        //TestClass.count++;
        //上面一行无法通过编译，因为 privage set 属性
	}
    for (int i=0;i<6;i++){
		p.Show();
		p = p.pre;
	}
    
    tryTestS1();
    
    string s = stdin.read_line();
    stdout.printf("%s\n",s);
}
