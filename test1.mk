CC = gcc
CFLAGS = -Wall `pkg-config --cflags gtk+-3.0`
LDFLAGS = -Wall `pkg-config --libs gtk+-3.0`
OBJS = test1.o
TARGET = test1

$(TARGET):$(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) -o $(TARGET) 

$(OBJS):%.o:%.c

clean:
	rm $(OBJS) $(TARGET)