#include <gtk/gtk.h>

 

int main(int argc,char *argv[ ])

{
	GtkWidget *window;
	GtkWidget *clist;
	gtk_init(&argc,&argv);
	window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect(GTK_OBJECT(window),"delete_event",GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
	gtk_widget_set_size_request(window,200,150);
	gtk_container_set_border_width(GTK_CONTAINER(window),10);
	clist=gtk_clist_new(3);
	gtk_clist_set_column_title(GTK_CLIST(clist),0,"姓名");
	gtk_clist_set_column_title(GTK_CLIST(clist),1,"姓别");
	gtk_clist_set_column_title(GTK_CLIST(clist),2,"年龄");
	gtk_clist_column_titles_show(GTK_CLIST(clist));
	gtk_container_add(GTK_CONTAINER(window),clist);
	gtk_widget_show(clist);
	gtk_widget_show(window);
	gtk_main();
}
