using Gtk;

Gtk.Application app;
int counter = 1;
public class App2:GLib.Object{
    Gtk.ApplicationWindow win1;
	public App2(){
		// Sets the title of the Window:
        win1 = new Gtk.ApplicationWindow(app);
        
		win1.title = "My Gtk.Window";

		// Center window at startup:
		win1.window_position = Gtk.WindowPosition.CENTER;

		// Sets the default size of a window:

		// Whether the titlebar should be hidden during maximization.
		win1.hide_titlebar_when_maximized = true;
        
        win1.set_resizable(false);
        
        win1.set_show_menubar(true);
        setup_dropbox();

		// Method called on pressing [X]
		win1.destroy.connect (() => {
			// Print "Bye!" to our console: 
			print ("Bye!\n");

			// Terminate the mainloop: (main returns 0)
			Gtk.main_quit ();
		});
	}
	public void setup_dropbox(){
		Gtk.Grid grid=new Gtk.Grid();
		Gtk.EventBox dropbox = new Gtk.EventBox();
		//Gtk.Entry dropbox = new Gtk.Entry();
		dropbox.set_size_request(200,200);
		win1.add(grid);
		grid.attach(dropbox,0,0);
		Gtk.Entry entry1 = new Gtk.Entry();
		grid.attach(entry1,0,1);
		
		Gtk.Label label1 = new Gtk.Label("\n\ndrop/paste file here\n\n");
		label1.selectable=true;
		dropbox.add(label1);
		//Gtk.TargetEntry d1= {"drag1",0,0};
		Gtk.drag_dest_set (dropbox, Gtk.DestDefaults.ALL, null, Gdk.DragAction.COPY);
		Gtk.drag_dest_add_uri_targets(dropbox);
		dropbox.drag_data_received.connect((context, x,y,data, info, time)=>{
			var uris = data.get_uris();
			if(uris.length>1){
				print("too many uris\n");
				return;
			}
			stdout.printf(@"recv: $(uris[0])\n");
			var fname = GLib.Filename.from_uri(uris[0]);
			stdout.printf(@"Path: $(fname)\n");
		});
		
		dropbox.button_press_event.connect((e)=>{
			print("button press\n");
			return true;
		});
		//var ag1 = new Gtk.AccelGroup ();
		//win1.add_accel_group(ag1);
		//dropbox.add_accelerator("key_press_event",ag1,Gdk.Key.v,Gdk.ModifierType.CONTROL_MASK,Gtk.AccelFlags.VISIBLE);
		//dropbox.set_events(Gdk.EventMask.KEY_PRESS_MASK|Gdk.EventMask.BUTTON_PRESS_MASK);
		dropbox.key_press_event.connect((e)=>{
			if(e.keyval==Gdk.Key.v && e.state==Gdk.ModifierType.CONTROL_MASK){
				print("press: ctrl-v\n");
				var clipboard1 = Gtk.Clipboard.@get(Gdk.Atom.NONE);
				clipboard1.request_uris((b, uris)=>{
					foreach(string uri1 in uris){
						print(uri1+"\n");
					}
				});
			}
			return true;
		});
	}
    public void setup_menubar(){
        var menu1 = new GLib.Menu();
        var item1 = new GLib.MenuItem("About","app.about");
        menu1.append_item(item1);
        
        item1 = new GLib.MenuItem("Notify","app.notify");
        menu1.append_item(item1);
        
        var menubar =new GLib.Menu();
        menubar.append_submenu("Help",menu1);
        
        var menu2 = new GLib.Menu();
        var item21 = new GLib.MenuItem("Open","app.open");
        menu2.append_item(item21);
        menubar.append_submenu("File",menu2);
        
        app.set_menubar(menubar as GLib.MenuModel);
        
        add_actions();
    }
    public void show(){
        win1.show_all();
    }
    private void add_actions () {
		SimpleAction act1 = new SimpleAction ("open", null);
		act1.activate.connect (() => {
			app.hold ();
			print ("Simple action %s activated\n", act1.get_name ());
			var dlg = new Gtk.FileChooserDialog ("Open File", win1 as Gtk.Window, Gtk.FileChooserAction.OPEN, 
				"Open",Gtk.ResponseType.ACCEPT, "Cancel",Gtk.ResponseType.CANCEL);
			var res = dlg.run();
			if (res == Gtk.ResponseType.ACCEPT){
				stdout.printf("%s\n",dlg.get_filename());
			}
			dlg.destroy();
			app.release ();
		});
        act1.set_enabled(true);
		app.add_action (act1);

		SimpleAction act2 = new SimpleAction ("about", null);
		act2.activate.connect (() => {
			app.hold ();
			print ("Simple action %s activated\n", act2.get_name ());
			app.release ();
		});
        act2.set_enabled(true);
		app.add_action (act2);
	}
}

public static int main(string[] args){
	Gtk.init(ref args);
    app = new Gtk.Application("test.gmenu",GLib.ApplicationFlags.FLAGS_NONE);
    app.register();
	var win = new App2();
    win.setup_menubar();
    win.show();
	Gtk.main ();
	return 0;
}
