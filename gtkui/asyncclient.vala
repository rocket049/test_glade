//client.vala
//valac --pkg jsonrpc-glib-1.0 jsonclient.vala
using Jsonrpc;

public class RpcClient:GLib.Object{
	bool rpcx {set;get;default=false;}
	public Jsonrpc.Client c;
	
	public owned void rpcConnect(string host,uint16 port){
		Resolver resolver = Resolver.get_default ();
		List<InetAddress> addresses = resolver.lookup_by_name (host, null);
		InetAddress address = addresses.nth_data (0);
		SocketClient client = new SocketClient ();
		SocketConnection conn = client.connect(new InetSocketAddress (address, port));
		c = new Jsonrpc.Client(conn);
	}
	public void get_text(string s,GLib.AsyncReadyCallback f){
		string[] v = {"Return:",s};
		var params = new Variant.strv(v);
		var thread = new GLib.Thread<int>("get text",()=>{
			//var loop = new GLib.MainLoop();
			c.call_async.begin("Arith.Hello",params,null,f);
			//loop.run();
			return 0;
		});
	}
	public void close(){
		c.close();
	}
}
