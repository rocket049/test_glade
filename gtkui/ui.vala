using Gtk;
using Gdk;
using Pango;

public class App1:Gtk.Window{
	public Gtk.ListBox list1;
    public MyBrowser browser = null;
    //RpcClient rpc1;
    Gtk.CssProvider css1;
    Gtk.ListBoxRow panel1;
    int counter = 0;
	public void RunApp(){
		//rpc1 = new RpcClient();
		//rpc1.rpcConnect("localhost",6666);
        this.browser = new MyBrowser();
		// Sets the title of the Window:
		this.title = "My Gtk.Window";

		// Center window at startup:
		this.window_position = Gtk.WindowPosition.CENTER;

		// Sets the default size of a window:
		this.set_default_size (350, 400);

		// Whether the titlebar should be hidden during maximization.
		this.hide_titlebar_when_maximized = true;
        
        this.set_resizable(false);
        this.set_icon_from_file(Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"tank.png"));

		// Method called on pressing [X]
		this.destroy.connect (() => {
			// Print "Bye!" to our console: 
			print ("Bye!\n");

			// Terminate the mainloop: (main returns 0)
			Gtk.main_quit ();
			//rpc1.close();
		});

		// Widget content:
        var scrollWin = new Gtk.ScrolledWindow(null,null);
        this.add(scrollWin);
		this.list1 = new Gtk.ListBox();
        
		scrollWin.add (this.list1);
		
		css1 = new Gtk.CssProvider();
		//var sc = this.get_style_context ();
		//sc.add_provider(css1,Gtk.STYLE_PROVIDER_PRIORITY_USER);
		css1.load_from_data("""grid{
	padding:5px 5px 5px 5px;
	background-color:#BABABA;
	color:#800000;
	font-size:18px;
}""");

        this.add_buttons();
        
//        this.add_left_name_icon("张三","tank.png");
        
//		this.add_image("test.png");
        
//        this.add_text("在linux下chromium浏览器点击thunder链接会显示使用xdg-open打开，之后则显示无法打开。在网上找了些方法都是要求修改xdg-open脚本的，其实完全没有必要，这个问题的根本原因是没有理解Linux桌面文件（XDG）的打开机制。");
        
//        add_right_name_icon("李四","tank.png");
        
//        add_image("test2.png");
        
//        add_text("图片说明",true);
        
//        this.add_text("在网上找了些方法");
        
		this.show_all();
		this.show.connect( ()=>{
			this.add_app_menu();
		} );
	}
	
	public void add_app_menu(){
		var menubar = new GLib.Menu();
		var menu1 = new GLib.Menu();
		menubar.append_section("File",menu1);
		menu1.append("Open","app.open");
		menu1 = new GLib.Menu();
		menubar.append_section("Help",menu1);
		menu1.append("About","app.about");
		this.application.set_menubar(menubar as GLib.MenuModel);
	}
    
    public void add_buttons(){
        var grid = new Gtk.Grid();
        grid.set_column_spacing(5);
        var b1 = new Gtk.Button.with_label (_("b1"));
        var b2 = new Gtk.Button.with_label ("b2");
        var b3 = new Gtk.Button.with_label ("b3");
        var b4 = new Gtk.Button.with_label ("b4");
        grid.attach(b1,0,0);
        grid.attach(b2,1,0);
        grid.attach(b3,2,0);
        grid.attach(b4,3,0);
        this.list1.add(grid);
        var r = grid.get_parent() as Gtk.ListBoxRow;
		r.name = @"$(counter)";
		counter+=1;
        
        this.panel1 = grid.get_parent() as Gtk.ListBoxRow;
        
        b1.clicked.connect (() => {
			// Emitted when the button has been activated:
			this.add_left_name_icon("张三",Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"tank.png"));
            this.add_image(Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"test.png"));
            this.add_text("知道这是什么游戏吗？",true);
            move_buttons();
            this.show_all();
            this.browser.open("http://www.baidu.cn");
		});
        b2.clicked.connect (() => {
			// Emitted when the button has been activated:
			this.add_right_name_icon("李四",Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"tank.png"));
            this.add_image(Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"test2.png"));
            this.add_text("知道这是什么吗？",true);
            move_buttons();
            this.show_all();
            this.browser.open("http://www.hao123.com");
		});
        b3.clicked.connect (() => {
			// Emitted when the button has been activated:
			this.add_left_name_icon("张三",Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"tank.png"));
            this.add_text("在linux下chromium浏览器点击thunder链接会显示使用xdg-open打开，之后则显示无法打开。在网上找了些方法都是要求修改xdg-open脚本的，其实完全没有必要，这个问题的根本原因是没有理解Linux桌面文件（XDG）的打开机制。");
            move_buttons();
            this.show_all();
		});
        b4.clicked.connect (() => {
			// Emitted when the button has been activated:
			this.add_left_name_icon("张三",Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"tank.png"));
            this.add_text("只能靠在网上找了些方法，其实完全没有必要。");
//            GLib.AsyncReadyCallback f = (o,r)=>{
//				GLib.Variant res1;
//				rpc1.c.call_async.end(r,out res1);
//				stdout.printf("async : %s\n",res1.get_string());
//				this.add_text( res1.get_string() );
//				this.show_all();
//			};
//			rpc1.get_text("rpc调用",f);
			move_buttons();
            this.show_all();
            //遍历 ListBoxRow
            this.list1.foreach( (r)=>{
				stdout.printf("name: %s\n",r.name);
			});
		});
    }
    
    public void move_buttons(){
		this.list1.remove( this.panel1 );
		this.list1.add(this.panel1);
	}
    
    public void add_right_name_icon(string name,string icon_path){
        var pix1 = new Gdk.Pixbuf.from_file(icon_path);
        var grid2 = new Gtk.Grid();
        var img2 = new Gtk.Image();
        img2.set_from_pixbuf(pix1);
        grid2.attach(img2,1,0);
        grid2.halign = Gtk.Align.END;
		var l2 = new Gtk.Label(name);
		l2.xalign = (float)1;
        grid2.attach(l2,0,0);
        grid2.set_column_spacing(5);
		this.list1.add(grid2);
		var sc = grid2.get_style_context ();
		sc.add_provider(css1,Gtk.STYLE_PROVIDER_PRIORITY_USER);
		var r = grid2.get_parent() as Gtk.ListBoxRow;
		r.name = @"$(counter)";
		counter+=1;
    }
    public void add_left_name_icon(string name,string icon_path){
        var grid1 = new Gtk.Grid();
        var pix1 = new Gdk.Pixbuf.from_file(icon_path);
        var img1 = new Gtk.Image();
        img1.set_from_pixbuf(pix1);
        grid1.attach(img1,0,0);
		var l1 = new Gtk.Label(name);
		l1.xalign = (float)0;
        grid1.attach(l1,1,0);
        grid1.set_column_spacing(5);
		this.list1.add(grid1);
		var sc = grid1.get_style_context ();
		sc.add_provider(css1,Gtk.STYLE_PROVIDER_PRIORITY_USER);
		var r = grid1.get_parent() as Gtk.ListBoxRow;
		r.name = @"$(counter)";
		counter+=1;
    }
    public void add_image(string pathname){
        var p1 = new Gdk.Pixbuf.from_file(pathname);
        var image = new Gtk.Image();
        if(p1.width>300){
            var xs = (double)300/(double)p1.width;
            var h2 = (int)(p1.height*xs);
            var p2 = new Gdk.Pixbuf(Gdk.Colorspace.RGB,true,8,300,h2);
            p1.scale(p2, 0, 0, 300, h2, 0.0, 0.0, xs, xs,Gdk.InterpType.NEAREST);
            image.set_from_pixbuf(p2);
        }else{
            image.set_from_pixbuf(p1);
        }
		this.list1.add(image);
		var r = image.get_parent() as Gtk.ListBoxRow;
		r.name = @"$(counter)";
		counter+=1;
    }
    public void add_text(string text,bool center=false){
        var lb = new Gtk.Label(text);
		lb.wrap = true;
        lb.wrap_mode = Pango.WrapMode.CHAR;
        if(!center){
            lb.xalign = (float)0;
        }
        lb.width_request = 300;
        lb.max_width_chars = 15;
        var grid=new Gtk.Grid();
        var lb1 = new Gtk.Label("");
        lb1.width_request = 5;
        grid.attach(lb1,0,0);
        grid.attach(lb,1,0);
        var lb2 = new Gtk.Label("");
        lb2.width_request = 5;
        grid.attach(lb2,2,0);
        grid.halign = Gtk.Align.CENTER;
		this.list1.add(grid);
		var r = grid.get_parent() as Gtk.ListBoxRow;
		r.name = @"$(counter)";
		counter+=1;
    }
}

public class MyGrid: Gtk.Grid{
	Gtk.ListBox friends;
	Gtk.ListBox msgs;
	Gtk.Entry entry1;
	public MyGrid(){
		this.set_column_spacing(5);
		Gdk.Color color1;
		//Gdk.Color.parse("#ABABAB",out color1);
		//this.modify_bg(Gtk.StateType.NORMAL,color1);
		Gdk.Color.parse("#FFFFFF",out color1);
		var cssp = new Gtk.CssProvider();
		var sc = this.get_style_context ();
		sc.add_provider(cssp,Gtk.STYLE_PROVIDER_PRIORITY_USER);
		cssp.load_from_data("""grid{
	padding:5px 5px 5px 5px;
	background-color:#BABABA;
	font-size:18px;
}""");
		
		var scrollWin1 = new Gtk.ScrolledWindow(null,null);
		scrollWin1.width_request = 150;
		scrollWin1.height_request = 270;
		this.attach(scrollWin1,0,0,2,2);
		this.friends = new Gtk.ListBox();
		this.friends.width_request = 150;
		this.friends.height_request = 270;
		scrollWin1.add(this.friends);
		this.friends.add(new Gtk.Label("friend 1"));
		this.friends.modify_bg(Gtk.StateType.NORMAL,color1);
		this.friends.border_width = 5;
		
		var b1 = new Gtk.Button.with_label("b1");
		this.attach(b1,0,2,1,1);
		
		var b2 = new Gtk.Button.with_label("b2");
		this.attach(b2,1,2,1,1);
		
		var b3 = new Gtk.Button.with_label("b3");
		this.attach(b3,2,0,1,1);
		
		var b4 = new Gtk.Button.with_label("b4");
		this.attach(b4,3,0,1,1);
		
		var b5 = new Gtk.Button.with_label("b5");
		this.attach(b5,4,0,1,1);
		
		var b6 = new Gtk.Button.with_label("b6");
		this.attach(b6,5,0,1,1);
		
		var scrollWin2 = new Gtk.ScrolledWindow(null,null);
		scrollWin2.width_request = 250;
		scrollWin2.height_request = 250;
		this.attach(scrollWin2,2,1,4,1);
		this.msgs = new Gtk.ListBox();
		this.msgs.width_request = 250;
		this.msgs.height_request = 250;
		this.msgs.border_width = 2;
		this.msgs.modify_bg(Gtk.StateType.NORMAL,color1);
		scrollWin2.add(this.msgs);
		this.msgs.add(new Gtk.Label("message 1"));
		this.msgs.set_css_name("label");
		
		var grid1 = new Gtk.Grid();
		this.attach(grid1,2,2,4,1);
		
		this.entry1 = new Gtk.Entry();
		grid1.attach(this.entry1,0,0,1,1);
		this.entry1.expand = true;
		
		var b7 = new Gtk.Button.with_label("b7");
		grid1.attach(b7,1,0,1,1);
	}
}

public class App2:Gtk.Window{
	public App2(){
		// Sets the title of the Window:
		this.title = "My Gtk.Window";

		// Center window at startup:
		this.window_position = Gtk.WindowPosition.CENTER;

		// Sets the default size of a window:

		// Whether the titlebar should be hidden during maximization.
		this.hide_titlebar_when_maximized = true;
        
        this.set_resizable(false);
        this.set_icon_from_file(Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"tank.png"));

		// Method called on pressing [X]
		this.destroy.connect (() => {
			// Print "Bye!" to our console: 
			print ("Bye!\n");

			// Terminate the mainloop: (main returns 0)
			Gtk.main_quit ();
		});
	}
}
public static string prog_path;
public void set_my_locale(string path1){
	var dir1 = Path.get_dirname(path1);
	prog_path = Path.build_path(Path.DIR_SEPARATOR_S,Environment.get_current_dir(),dir1);
	var textpath = Path.build_path(Path.DIR_SEPARATOR_S,prog_path,"locale");
	GLib.Intl.textdomain("ui");
	GLib.Intl.bindtextdomain("ui","/home/fhz8/gitee/test_glade/gtkui/locale");
}
public static int main(string[] args){
	set_my_locale(args[0]);
	Gtk.init(ref args);
	
	//var app = new App2();
	//var grid1 = new MyGrid();
	//app.add(grid1);
	var app = new App1();
	app.RunApp();
	app.show_all();
	Gtk.main ();
	return 0;
}
