using Gtk;
//using Notify;

Gtk.Application app;
int counter = 1;
public class App2:GLib.Object{
    Gtk.ApplicationWindow win1;
	public App2(){
		// Sets the title of the Window:
        win1 = new Gtk.ApplicationWindow(app);
        
		win1.title = "My Gtk.Window";

		// Center window at startup:
		win1.window_position = Gtk.WindowPosition.CENTER;

		// Sets the default size of a window:

		// Whether the titlebar should be hidden during maximization.
		win1.hide_titlebar_when_maximized = true;
        
        win1.set_resizable(false);
        
        win1.set_show_menubar(true);
        setup_dropbox();

		// Method called on pressing [X]
		win1.destroy.connect (() => {
			// Print "Bye!" to our console: 
			print ("Bye!\n");

			// Terminate the mainloop: (main returns 0)
			Gtk.main_quit ();
		});
	}
	public void setup_dropbox(){
		Gtk.Grid grid=new Gtk.Grid();
		Gtk.EventBox dropbox = new Gtk.EventBox();
		//Gtk.Entry dropbox = new Gtk.Entry();
		dropbox.set_size_request(200,200);
		win1.add(grid);
		grid.attach(dropbox,0,0);
		Gtk.Entry entry1 = new Gtk.Entry();
		grid.attach(entry1,0,1);
		
		Gtk.Label label1 = new Gtk.Label("\n\ndrop/paste file here\n\n");
		label1.selectable=true;
		dropbox.add(label1);
		//Gtk.TargetEntry d1= {"drag1",0,0};
		Gtk.drag_dest_set (dropbox, Gtk.DestDefaults.ALL, null, Gdk.DragAction.COPY);
		Gtk.drag_dest_add_uri_targets(dropbox);
		dropbox.drag_data_received.connect((context, x,y,data, info, time)=>{
			var uris = data.get_uris();
			if(uris.length>1){
				print("too many uris\n");
				return;
			}
			stdout.printf(@"recv: $(uris[0])\n");
			var fname = GLib.Filename.from_uri(uris[0]);
			stdout.printf(@"Path: $(fname)\n");
		});
		
		dropbox.button_press_event.connect((e)=>{
			print("button press\n");
			return true;
		});
		//var ag1 = new Gtk.AccelGroup ();
		//win1.add_accel_group(ag1);
		//dropbox.add_accelerator("key_press_event",ag1,Gdk.Key.v,Gdk.ModifierType.CONTROL_MASK,Gtk.AccelFlags.VISIBLE);
		//dropbox.set_events(Gdk.EventMask.KEY_PRESS_MASK|Gdk.EventMask.BUTTON_PRESS_MASK);
		dropbox.key_press_event.connect((e)=>{
			if(e.keyval==Gdk.Key.v && e.state==Gdk.ModifierType.CONTROL_MASK){
				print("press: ctrl-v\n");
				var clipboard1 = Gtk.Clipboard.@get(Gdk.Atom.NONE);
				clipboard1.request_uris((b, uris)=>{
					foreach(string uri1 in uris){
						print(uri1+"\n");
					}
				});
			}
			return true;
		});
	}
    public void setup_menubar(){
        var menu1 = new GLib.Menu();
        var item1 = new GLib.MenuItem("About","app.about");
        menu1.append_item(item1);
        
        item1 = new GLib.MenuItem("Notify","app.notify");
        menu1.append_item(item1);
        
        var menubar =new GLib.Menu();
        menubar.append_submenu("Help",menu1);
        
        var menu2 = new GLib.Menu();
        var item21 = new GLib.MenuItem("Open","app.open");
        menu2.append_item(item21);
        menubar.append_submenu("File",menu2);
        
        app.set_menubar(menubar as GLib.MenuModel);
        
        add_actions();
    }
    public void show(){
        win1.show_all();
    }
    private void add_actions () {
		SimpleAction act1 = new SimpleAction ("open", null);
		act1.activate.connect (() => {
			app.hold ();
			print ("Simple action %s activated\n", act1.get_name ());
			app.release ();
		});
        act1.set_enabled(true);
		app.add_action (act1);

		SimpleAction act2 = new SimpleAction ("about", null);
		act2.activate.connect (() => {
			app.hold ();
			print ("Simple action %s activated\n", act2.get_name ());
			//notify_some("About");
			app.release ();
		});
        act2.set_enabled(true);
		app.add_action (act2);
		
		SimpleAction act3 = new SimpleAction ("notify", null);
		act3.activate.connect (() => {
			app.hold ();
			print ("Simple action %s activated\n", act3.get_name ());
			var notify1 = new GLib.Notification(@"Sample notify $(counter)");
			counter++;
			notify1.set_body("Do something");
			try{
				File fp1 = File.new_for_path("./tank.png");
				var info1 = fp1.query_info ("standard::icon", 0);
				var icon1 = info1.get_icon();
				notify1.set_icon(icon1);
			}catch (Error e) {
				print ("Error: %s\n", e.message);
			}
			notify1.add_button("about","app.about");
			notify1.set_default_action("app.about");
			app.send_notification(null,notify1);
			app.release ();
		});
        act3.set_enabled(true);
		app.add_action (act3);
	}
}
//public void notify_some(string body){
//	string summary = "Short summary";
//	string icon = "dialog-information";
//	Notify.init ("My test app");
//	try {
//		Notify.Notification notification = new Notify.Notification (summary, body, icon);
//		notification.add_action ("action-name", "Quit", (notification, action) => {
//			print ("notify action!\n");
//		});
//		notification.show ();
//	} catch (Error e) {
//		error ("Error: %s", e.message);
//	}
//}
public void set_statusicon(){
	Gtk.StatusIcon icon1 = new Gtk.StatusIcon.from_file("ex1.png");
	//security-low
	icon1.set_visible(true);
	icon1.popup_menu.connect((bt,atime)=>{
		Gtk.Menu menu1=new Gtk.Menu();
		Gtk.MenuItem item1 = new Gtk.MenuItem.with_label("item1");
		
		item1.activate.connect(()=>{
			stdout.printf("menu item1 activate\n");
		});
		
		menu1.append(item1);
		menu1.show_all();
		
		menu1.popup(null,null,icon1.position_menu,bt,atime);
	});
}
public static int main(string[] args){
	Gtk.init(ref args);
    app = new Gtk.Application("test.gmenu",GLib.ApplicationFlags.FLAGS_NONE);
    app.register();
	var win = new App2();
    win.setup_menubar();
    win.show();
    set_statusicon();
	Gtk.main ();
	return 0;
}
